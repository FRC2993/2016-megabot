#include "ClimbingArm.h"

// CAN ids
#define CAN_ID_TALON_CLIMBING_ARM_1 7
#define CAN_ID_TALON_CLIMBING_ARM_2 8
#define CAN_ID_SOLENOID_CLIMBING_ARM_RELEASE 9
// Joystick port for climbing arm control
#define JOYSTICK_PORT_CLIMBING_ARM 1
// Joystick button to use for releasing the climbing arm
#define JOYSTICK_BUTTON_RELEASE_CLIMBING_ARM 0
// PCM solenoid port for the climbing arm release mechanism
#define PCM_PORT_SOLENOID_CLIMBING_ARM_RELEASE 2
// Climbing arm voltage ramp rate (volts/sec)
#define CLIMBING_ARM_VOLTAGE_RAMP_RATE 6.0
// Logical value to set the solenoid to in order to release the arm
#define RELEASE true
// Logical value to set the arm motors to in order to extend the arm
#define EXTEND 1.0
// Logical value to set the arm motors to in order to retract the arm
#define RETRACT =1.0

ClimbingArm::ClimbingArm()
{
    pJoystick = new Joystick(JOYSTICK_PORT_CLIMBING_ARM);

    pSolenoid = new Solenoid(CAN_ID_SOLENOID_CLIMBING_ARM_RELEASE, PCM_PORT_SOLENOID_CLIMBING_ARM_RELEASE);

    pTalon1 = new CANTalon(CAN_ID_TALON_CLIMBING_ARM_1);
    pTalon2 = new CANTalon(CAN_ID_TALON_CLIMBING_ARM_2);

    pTalon1->SetVoltageRampRate(CLIMBING_ARM_VOLTAGE_RAMP_RATE);
    pTalon2->SetVoltageRampRate(CLIMBING_ARM_VOLTAGE_RAMP_RATE);

    armReleased = false;

    pTalon1->Enable();
    pTalon2->Enable();
}

ClimbingArm::~ClimbingArm()
{
}

void ClimbingArm::Update()
{
    if (pJoystick->GetRawButton(JOYSTICK_BUTTON_RELEASE_CLIMBING_ARM))
    {
        ReleaseArm();
    }

    if (armReleased)
    {
        float x = pJoystick->GetX();
        pTalon1->Set(x);
        pTalon2->Set(x);
    }
}

void ClimbingArm::ReleaseArm()
{
    if (!armReleased)
    {
        pSolenoid->Set(RELEASE);
        armReleased = true;
    }
}
