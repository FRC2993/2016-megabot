#ifndef _INCLUDED_CLAW_H_
#define _INCLUDED_CLAW_H_

#include "Joystick.h"
#include "CANTalon.h"
#include "PIDController.h"

class Claw
{
private:
    Joystick* pJoystick;
    CANTalon* pTalonArm;
    CANTalon* pTalonHand1;
    CANTalon* pTalonHand2;
    PIDController* pPidController;

public:
    Claw();

    ~Claw();

    void Update();

    float ConvertThrottleToVoltage();
};

#endif
