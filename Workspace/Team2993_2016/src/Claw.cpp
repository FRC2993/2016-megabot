#include "Claw.h"

// CAN ids
#define CAN_ID_TALON_CLAW_ARM 10
#define CAN_ID_TALON_CLAW_HAND1 11
#define CAN_ID_TALON_CLAW_HAND2 12
// PID values for arm angle controller
#define CLAW_kP 0.0
#define CLAW_kI 0.0
#define CLAW_kD 0.0
// The potentiometer voltage reading when arm is all the way down
#define CLAW_ARM_DOWN_VOLTAGE 0
// The potentiometer voltage reading when arm is all the way up
#define CLAW_ARM_UP_VOLTAGE 0
// Joystick port for claw control
#define JOYSTICK_PORT_CLAW 1
// Joystick button to tell claw hand to grab a ball
#define JOYSTICK_BUTTON_CLAW_HAND_GRAB 1
// Joystick button to tell claw hand to release a ball
#define JOYSTICK_BUTTON_CLAW_HAND_RELEASE 2
// Voltage ramp rate (volts/sec) for arm control
#define CLAW_ARM_VOLTAGE_RAMP_RATE 6
// Voltage ramp rate (volts/sec) for claw hand control
#define CLAW_HAND_VOLTAGE_RAMP_RATE 6
// Logical value to feed to motor when claw hand is to grab
#define CLAW_HAND_GRAB 1.0
// Logical value to feed to motor when claw hand is to release
#define CLAW_HAND_RELEASE 1.0

Claw::Claw()
{
    pJoystick = new Joystick(JOYSTICK_PORT_CLAW);
    pTalonArm = new CANTalon(CAN_ID_TALON_CLAW_ARM);
    pTalonHand1 = new CANTalon(CAN_ID_TALON_CLAW_HAND1);
    pTalonHand2 = new CANTalon(CAN_ID_TALON_CLAW_HAND2);

    pTalonArm->SetVoltageRampRate(CLAW_ARM_VOLTAGE_RAMP_RATE);
    pTalonHand1->SetVoltageRampRate(CLAW_HAND_VOLTAGE_RAMP_RATE);
    pTalonHand2->SetVoltageRampRate(CLAW_HAND_VOLTAGE_RAMP_RATE);

    pTalonArm->SetControlMode(CANSpeedController::kPosition);
    pTalonArm->SetSetpoint(CLAW_ARM_UP_VOLTAGE);

    pPidController = new PIDController(CLAW_kP, CLAW_kI, CLAW_kD, pTalonArm, pTalonArm);
    pPidController->Enable();
    pPidController->SetContinuous(false);

    pTalonArm->Enable();
    pTalonHand1->Enable();
    pTalonHand2->Enable();
    pPidController->Enable();
}

Claw::~Claw()
{
}

void Claw::Update()
{
    float desiredVoltage = ConvertThrottleToVoltage();

    std::cout << "Arm angle actual/desired voltage = " <<
        pTalonArm->GetAnalogIn() << " / " << desiredVoltage << std::endl;

    if (pJoystick->GetRawButton(JOYSTICK_BUTTON_CLAW_HAND_GRAB))
    {
        pTalonHand1->Set(CLAW_HAND_GRAB);
        pTalonHand2->Set(CLAW_HAND_GRAB);
    }
    else
    {
        pTalonHand1->Set(0);
        pTalonHand2->Set(0);
    }
    if (pJoystick->GetRawButton(JOYSTICK_BUTTON_CLAW_HAND_RELEASE))
    {
        pTalonHand1->Set(CLAW_HAND_RELEASE);
        pTalonHand2->Set(CLAW_HAND_RELEASE);
    }
    else
    {
        pTalonHand1->Set(0);
        pTalonHand2->Set(0);
    }

    pTalonArm->SetSetpoint(ConvertThrottleToVoltage());
}

float Claw::ConvertThrottleToVoltage()
{
    // Throttle down (0) = arm down = xx volts
    // Throttle up (1) = arm up = yy volts
    //
    // voltage = xx + (yy - xx) * throttle

    float clawArmDownVoltage = CLAW_ARM_DOWN_VOLTAGE;
    float clawArmUpVoltage = CLAW_ARM_UP_VOLTAGE;
    float voltage = clawArmDownVoltage + (clawArmUpVoltage - clawArmDownVoltage) * pJoystick->GetThrottle();
    return voltage;
}

